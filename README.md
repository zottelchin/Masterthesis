#  Master Thesis

This repo contains the results, tooling for my master thesis. The source for my thesis paper can be found at https://codeberg.org/zottelchin/Masterarbeit.

---- 
Phillipp Engelke<br>
Otto-von-Guericke-University Magdeburg<br>
09.2022