module workbalancer

go 1.16

require (
	github.com/containerd/containerd v1.6.1 // indirect
	github.com/docker/distribution v2.8.1+incompatible // indirect
	github.com/docker/docker v20.10.13+incompatible
	github.com/gammazero/workerpool v1.1.2
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/moby/term v0.0.0-20210619224110-3f7ff695adc6 // indirect
	github.com/opencontainers/image-spec v1.0.2 // indirect
	github.com/otiai10/copy v1.7.0
	github.com/rs/zerolog v1.26.1 // indirect
	golang.org/x/time v0.0.0-20220224211638-0e9765cccd65 // indirect
	google.golang.org/grpc v1.45.0 // indirect
	gotest.tools/v3 v3.1.0 // indirect
)
