module AprioriMA

go 1.16

require (
	github.com/eMAGTechLabs/go-apriori v1.0.0
	github.com/jackc/pgx/v4 v4.16.1
	github.com/manifoldco/promptui v0.9.0
)
