# Results

This folder contains all tables from the experiment. Tables ending with `_rules` contain data resulting from the association rule mining. Tables without this suffix, are the result from the data aggregation.

All tables where produced using pg_dump and can be imported using psql. For more information see: https://www.postgresql.org/docs/8.1/backup.html#BACKUP-DUMP-RESTORE